#include "HX711.h"
#define calibration_factor 29150 //This value is obtained using the HX711_load_cell_calibration sketch
#define LOADCELL_DOUT_PIN  3
#define LOADCELL_SCK_PIN  2
#define DELAY_INTERVAL 250
unsigned long lastExecutedMillis = 0; // vairable to save the last executed time for load cell code
HX711 scale;

void setup() {
  Serial.begin(9600);
  Serial.println("HX711 scale demo");

  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  scale.set_scale(calibration_factor); //This value is obtained by using the SparkFun_HX711_Calibration sketch
  scale.tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0

  Serial.println("Readings:");
}

void loop() {

  unsigned long currentMillis = millis();

  if (currentMillis - lastExecutedMillis >= DELAY_INTERVAL) {
    lastExecutedMillis = currentMillis; // save the last executed time

  Serial.print(453.59237*scale.get_units(), 0);  
  Serial.print(" gr");
  Serial.println();
  }



}
