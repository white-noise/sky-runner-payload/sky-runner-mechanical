clear all
clc

data = importdata('data.mat');
x = data(:,2);
y = data(:,1);

expfn = @(p,xd) p(1)*exp(p(2)*xd);  %define exponential function
errfn = @(p) sum((expfn(p,x)-y).^2);  %define sum-squared error
pfit = fminsearch( errfn, [0 0]);     %run the minimizer
plot(x,y,'bo');  hold on;   %plot your raw data
plot(x, expfn(pfit, x), 'r-');  %plot the fit data