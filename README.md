
# Sky runner 

Sky runner is a 1U CubeSat form payload that utilizes a passive autogyro descent system. An active control system is used. It consists of three control fins attached to the payload in a circular pattern around its axis. The system objective is to eliminate the spinning of the body frame and the oscillations in the orientation caused by the gyroscopic precession and external disturbances. 


<img src="skyrunner.png" height=250 />



- [ ] All the calculations and mathematical modeling are performed in _GNU Octave 6.3.0/MATLAB_
- [ ] Cad models are made in _Fusion 360_.

