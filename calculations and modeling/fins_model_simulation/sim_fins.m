%% Calcualate moment 

%Inputs:
u_inf_scalar = 9;
rpy = [0,0,0]; % roll,pitch yaw (Euler angles) in rad
fin_angles =[-2, 2, 7,-7]; % in degrees

%for Mz MAX: fin_angles =[10, 10, 10,10] -> TMOMENT = [0, 0, 0.0192]
%for Mx MAX: fin_angles =[0, 10, 0,-10] -> TMOMENT = [0.0147,0, 0]
%for My MAX: fin_angles =[-10, 0, 10,0] -> TMOMENT = [ 0, 0.0147, 0]

[TMOMENT,TLIFT,TDRAG] = calcMoments(u_inf_scalar,fin_angles, rpy)

