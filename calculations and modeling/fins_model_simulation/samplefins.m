N = 10;
X = zeros((2*N + 1)^4, 4);
Y = zeros((2*N + 1)^4, 3);

u_inf_scalar = 9;
rpy = [0,0,0]; % roll,pitch yaw (Euler angles) in rad

counter = 1;
for phi_1 = -N : N
    for phi_2 = -N : N
        for phi_3 = -N: N
            for phi_4 = -N: N
                Y(counter, :) = calcMoments(u_inf_scalar,[phi_1 phi_2 phi_3 phi_4], rpy);
                X(counter, :) = [phi_1 phi_2 phi_3 phi_4];
                counter = counter + 1;
            end
        end
    end
end

%writematrix(Y,'moments.csv');
%writematrix(X,'angles.csv');
